import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchTodo, deleteTodo } from '../actions';

class TodosShow extends Component {
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetchTodo(id);
    }

    onDeleteClick() {
        const { id } = this.props.match.params;
        this.props.deleteTodo(id, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const { todo } = this.props;

        if (!todo) {
            return <div>Loading...</div>
        }
        
        return (
            <div>
                <Link to="/">Back to Index</Link>
                <button
                    className="btn btn-danger pull-xs-right"
                    onClick={this.onDeleteClick.bind(this)}>
                    Delete Todo
                </button>
                <h3>{ todo.task }</h3>
                <h6>Description:</h6>
                <p>{todo.description}</p>
            </div>
        )
    }
}

function mapStateToProps({ todos }, ownProps) {
    return { todo: todos[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchTodo, deleteTodo })(TodosShow);
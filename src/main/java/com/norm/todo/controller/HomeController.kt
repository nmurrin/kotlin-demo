package com.norm.todo.controller

import org.springframework.web.bind.annotation.RequestMapping

class HomeController {

    @RequestMapping(value = *arrayOf("/", "/todos/**"))
    fun index(): String {
        return "index"
    }
}
package com.norm.todo.controller

import com.norm.todo.model.ToDo
import com.norm.todo.repository.ToDoRepository
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("\${spring.data.rest.base-path}")
class ToDoController {

    val todoRepository: ToDoRepository

    constructor(todoRepository: ToDoRepository) {
        this.todoRepository = todoRepository
    }

    @RequestMapping("/todo")
    fun getToDoList(): List<ToDo> {
        return todoRepository.findAll()
    }

    @RequestMapping("/todo/{id}")
    fun getToDo(@PathVariable("id") todoId: String): ToDo {
        return todoRepository.findById(todoId).get()
    }

    @RequestMapping(value = ["/todo"], method = arrayOf(RequestMethod.POST))
    fun saveToDo(@RequestBody todo: ToDo): ToDo {
        return todoRepository.save(todo)
    }

    @RequestMapping(value = ["/todo/{id}"], method = arrayOf(RequestMethod.DELETE))
    fun deletToDo(@PathVariable("id") todoId: String) {
        todoRepository.deleteById(todoId)
    }
}
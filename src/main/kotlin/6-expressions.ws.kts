
val a = 10
val b = 20
// if / else is an expression and can return a value
val x = if (a == b) 30 else 40
println(x)

val someString: String? = null

// try / catch is a expression and can return a value
// Have you had to initialize a var outside of try catch in Java?
val y = try {
    someString!!.length
} catch (x: Exception) {
    0
}
println("Length is: $y")

// Kotlin
fun myStringLength(input: String?): Int {
    return try {
        input!!.length
    } catch(x: Exception) {
        0
    }
}
println("Length is: ${myStringLength(someString)}")

// Java style
fun myStringLength2(input: String?): Int {
    var retVal = 0;
    try {
        retVal == input!!.length
    } catch (x: Exception) {
        //println("It was null")
    }
    return retVal
}
println("Length is ${myStringLength2(someString)}")
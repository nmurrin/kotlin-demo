// Named params, default values
fun multiply(a: Int = 1, b: Int): Int {
    return a * b
}
multiply(4, 5)

// Return keyword & type not needed, single line
fun multiply2(a: Int = 1, b: Int) = a * b

// List declaration, immutable
val myNumbers = listOf(1, 2, 3, 4, 5)
// Mutable form with type
val mutableList = mutableListOf<String>()

// Filter a list
val moreNumbers = 1..100
val myOddNumber = moreNumbers.filter { it % 2 == 1 }
println(myOddNumber)
val secondOddSquare = moreNumbers
        .map { it * it }
        .filter { it % 2 == 1 }
        .drop(2)
        .first()
println(secondOddSquare)

// Notice there was no stream() and no collect() above?
// GOTCHA - iterables are not lazy...but sequences are

// More Ranges
println((1..99 step 2).toList())
println((99 downTo 1 step 2).toList())

// Maps and map declaration
val myMap = mutableMapOf(
        "a" to 1,
        "b" to 2,
        "c" to 3)
println(myMap["b"])
myMap["d"] = 4

// Check for presence
println("Is a in map: ${"a" in myMap}")
println("Is e in map: ${"e" in myMap}")

// Creating a singleton
object Resource {
    val name = "Name"
}
println(Resource.name)




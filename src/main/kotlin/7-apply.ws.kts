data class User(val username: String, val password: String) {
    var groups: List<String> = listOf()
}

val user1 = User("nh123456", "RevLab$$").apply {
    groups = listOf("hertz", "dollar")
}

val user2 = User("nh123456", "RevLab$$")
user2.groups = listOf("hertz", "dollar")



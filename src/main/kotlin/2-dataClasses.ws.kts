// Data classes
// getters, setters, hashCode(), equals(), and toString()
data class User(val username: String, val password: String = "RevLab!!")

// Named variables replace builders
val myUser = User("nh102579")

// Copy and Mutation via copy()
val newUser = myUser.copy(username = "nh123456")
val myUserCopy = myUser.copy()

// Using Kotlin String Templates
println("We get toString for free: $myUser")

// data class toString
println("myUser = ${myUser}")

// Equals and Hashcode
// == does what we want - (no need for equals())
println("myUser == myUserCopy: ${myUser == myUserCopy}")

// Uses kotlin reference equality
println("myUser === myUserCopy: ${myUser === myUserCopy}")

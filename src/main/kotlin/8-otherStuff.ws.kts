// GOTCHA - ObjectMapper
// Likely to need an instance of ObjectMapper for Kotlin in your spring apps
// import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
// val mapper = jacksonObjectMapper()


// Static?
// Constructors?

class User(val username: String, val password: String) {
    var groups: List<String> = listOf()

    constructor(username: String,
                password: String = WELCOME_PASSWORD,
                groups: List<String> = listOf())

    // Class level stuff goes here, including functions
    companion object {
        // compile time constant
        const val WELCOME_PASSWORD  = "Welcome2Hertz!"
    }
}


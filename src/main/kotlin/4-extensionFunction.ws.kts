val moreNumbers = 1..100

fun Int.isOdd(): Boolean {
    return this % 2 == 1
}

// Filter a list
val myOddNumber = moreNumbers.filter { it.isOdd() }
println(myOddNumber)


# kotlin-demo
To run the Spring application will need:
- docker
- java 8+
- node / npm

# Run local mongo server (needed for application)
# -d = daemon mode (run in background)
docker pull mongo
docker run --name mongo -d -p 27017:27017 mongo

# Run the npm install locally to load javascript modules
npm install

# Package javascript files into Java project
./gradlew bundleJs

# Run the java project
./gradlew bootRun
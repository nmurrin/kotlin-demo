module.exports = {
  entry: {
    main: './src/main/js/index.js'
  },
  output: {
    path: __dirname,
    publicPath: '/',
    filename: './src/main/resources/static/bundle.js'
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  }
};
